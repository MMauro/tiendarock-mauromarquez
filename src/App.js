import "./App.css";
import { ThemeProvider } from "@material-ui/core/styles";
import theme from "./temaConfig";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import { NavBar } from "./components/NavBar";
import ItemListContainer from "./components/ItemListContainer";
import { ItemDetailContainer } from "./components/ItemDetailContainer";
import { CartProvider } from "./context/CartContext";
import Cart from "./components/Cart";
import { datos } from "./data/datos";
import db from "./firebase/firebase";
import { collection, addDoc } from "firebase/firestore";
import { fileUpload } from "./firebase/fileUpload";
import Form from "./components/Form";
import { Container, Grid } from "@mui/material";
import Footer from "./components/Footer";

function App() {
  const arrayUpload = () => {
    datos.forEach(async (element) => {
      const imgURL = await fileUpload(element.image);

      addDoc(collection(db, "products"), { ...element, image: imgURL });
    });
  };

  return (
    <CartProvider>
      <ThemeProvider theme={theme}>
        <BrowserRouter>
          <NavBar />
          {/* <button onClick={arrayUpload}>Subir items</button> */}
          <Routes>
            <Route
              path="/"
              element={<ItemListContainer greeting={"Nuestros Productos"} />}
            />

            <Route path="/category/:catId" element={<ItemListContainer />} />

            <Route
              path="/product/:itemId"
              element={
                <ItemDetailContainer greeting={"Detalles del Producto"} />
              }
            />

            <Route path="*" element={<Navigate to="/" />} />
            <Route path="/cart" element={<Cart />} />
            <Route path="/finalizarCompra" element={<Form />} />
          </Routes>
          <Footer />
        </BrowserRouter>
      </ThemeProvider>
    </CartProvider>
  );
}

export default App;
