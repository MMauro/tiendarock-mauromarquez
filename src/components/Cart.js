import React, { useContext } from "react";
import { CartContext } from "../context/CartContext";
import { Link } from "react-router-dom";
import Button from "@mui/material/Button";
import DeleteIcon from "@mui/icons-material/Delete";
import SendIcon from "@mui/icons-material/Send";
import { makeStyles } from "@material-ui/core/styles";
import HighlightOffSharpIcon from "@mui/icons-material/HighlightOffSharp";
import Stack from "@mui/material/Stack";
import Table from "@mui/material/Table";
import {
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
} from "@mui/material";

const useStyles = makeStyles((theme) => ({
  table: {
    minWidth: 650,
  },
  tableConainer: {
    borderRadius: 15,
    margin: "10px 10px",
    maxWidth: 950,
  },
  tableHeaderCell: {
    fontWeight: "bold",
    backgroundColor: theme.palette.secondary.light,
    color: theme.palette.getContrastText(theme.palette.secondary.dark),
  },
}));

const Cart = () => {
  const { items, removeItem, clearItem, total } = useContext(CartContext);
  const classes = useStyles();

  return (
    <TableContainer component={Paper} className={classes.tableConatianer}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell className={classes.tableHeaderCell}>Producto</TableCell>
            <TableCell className={classes.tableHeaderCell}>Cantidad</TableCell>
            <TableCell className={classes.tableHeaderCell}>
              Precio por Unidad
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {items.map((row) => (
            <TableRow key={row.id}>
              <TableCell>{row.title}</TableCell>
              <TableCell>{row.cantidad}</TableCell>
              <TableCell>${row.price}</TableCell>
              <HighlightOffSharpIcon
                fontSize="medium"
                onClick={() => removeItem(row.id)}
                style={{ cursor: "pointer" }}
              />
            </TableRow>
          ))}
          <TableRow>
            <TableCell rowSpan={1} />
            <TableCell colSpan={0}>Total</TableCell>
            <TableCell>${total}</TableCell>
          </TableRow>
          <Stack
            direction="row"
            spacing={2}
            style={{ justifyContent: "center  " }}
          >
            <Button
              variant="outlined"
              startIcon={<DeleteIcon />}
              onClick={() => clearItem()}
            >
              Vaciar Carrito
            </Button>
            <Button
              variant="contained"
              endIcon={<SendIcon />}
              component={Link}
              to={items.length === 0 ? "*" : "/finalizarCompra"}
            >
              Finalizar Compra
            </Button>
          </Stack>
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default Cart;
