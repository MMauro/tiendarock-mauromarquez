import LocalGroceryStoreTwoToneIcon from "@mui/icons-material/LocalGroceryStoreTwoTone";
import Badge from "@mui/material/Badge";
import React, { useContext } from "react";
import { CartContext } from "../context/CartContext";

const CartWidget = () => {
  const { items } = useContext(CartContext);
  let itemsInCart = 0;
  console.log(items);

  items.map((items) => {
    itemsInCart = itemsInCart + items.cantidad;
    // console.log(items.cantidad);
  });
  return (
    <div>
      <Badge color="secondary" badgeContent={itemsInCart}>
        <LocalGroceryStoreTwoToneIcon />
      </Badge>
    </div>
  );
};

export default CartWidget;
