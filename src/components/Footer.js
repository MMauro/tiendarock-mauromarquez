import Box from "@mui/material/Box";
import { Container, Grid } from "@mui/material";
import Link from "@mui/material/Link";
import theme from "../temaConfig";

export default function Footer() {
  return (
    <footer>
      <Box
        px={{ xs: 3, sm: 10 }}
        py={{ xs: 3, sm: 10 }}
        bgcolor="text.secondary"
        color="white"
      >
        <Container maxWidth="lg">
          <Grid container spacing={5}>
            <Grid item xs={12} sm={6}>
              <Box borderBottom={1}> Tienda-Rock</Box>
              <Box>
                <Link href="/" color="inherit">
                  Productos
                </Link>
              </Box>
              <Box>
                <Link href="/cart" color="inherit">
                  Carrito
                </Link>
              </Box>
            </Grid>
          </Grid>

          <Box textAlign="center" pt={{ xs: 5, sm: 10 }} pb={{ xs: 5, sm: 0 }}>
            Dev Mauro Alejandro Marquez &reg; {new Date().getFullYear()}
          </Box>
        </Container>
      </Box>
    </footer>
  );
}
