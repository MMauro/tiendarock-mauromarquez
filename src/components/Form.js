import React, { useState, useContext } from "react";
import { CartContext } from "../context/CartContext";
import Input from "@mui/material/Input";
import Button from "@mui/material/Button";
import { collection, addDoc } from "firebase/firestore";
import db from "../firebase/firebase";
import { Container } from "@mui/material";

const Form = () => {
  const { items, clearItem, total } = useContext(CartContext);

  const [compraOk, setCompraOk] = useState(false);

  const [loading, setLoading] = useState(true);

  const [err, setErr] = useState({
    errName: false,
    errLastName: false,
    errPhone: false,
    errEmail: false,
  });

  const [formData, setFormData] = useState({
    name: "",
    lastName: "",
    phone: "",
    email: "",
  });

  const handleChange = (event) => {
    event.preventDefault();
    const { name, value } = event.target;
    setFormData((prevFormData) => ({
      ...prevFormData,
      [name]: value,
    }));
  };

  const cancel = () => {
    clearItem();
    setFormData({
      name: "",
      lastName: "",
      phone: "",
      email: "",
    });
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    let ordenCompra = {
      buyer: {
        name: formData.name,
        lastName: formData.lastName,
        phone: formData.phone,
        email: formData.email,
      },
      items: { ...items },
      total: total,
    };

    if (formData.name.length < 3) {
      setErr((prevErr) => ({ ...prevErr, errName: true }));
    } else if (formData.lastName.length < 3) {
      setErr((prevErr) => ({ ...prevErr, errLastName: true }));
    } else if (formData.phone.length < 8) {
      setErr((prevErr) => ({ ...prevErr, errPhone: true }));
    } else if (formData.email.length === 0) {
      setErr((prevErr) => ({ ...prevErr, errEmail: true }));
    } else {
      addDoc(collection(db, "orders"), { ordenCompra });
      setCompraOk(true);
    }
  };

  return (
    <Container sx={{ width: 900 }}>
      <div>
        <h2>Detalle de Pedido</h2>
        <ul>
          {items.map((item) => {
            return (
              <li
                key={item.id}
              >{`Producto: ${item.title} - Precio: $${item.price} - Cantidad: x${item.cantidad}`}</li>
            );
          })}
        </ul>
      </div>
      <form onSubmit={handleSubmit}>
        <h2>Información Personal</h2>
        <p>Por favor complete el formulario para realizar su pedido.</p>
        <div>
          {formData.name.length < 3 && formData.name.length >= 1 ? (
            <p>* El Nombre debe tener al menos 3 caracteres.</p>
          ) : (
            <></>
          )}
          <Input
            type="text"
            placeholder="Nombre"
            name="name"
            onChange={handleChange}
            value={formData.name}
          />
        </div>
        <div>
          {formData.lastName.length < 3 && formData.lastName.length >= 1 ? (
            <p>* El Apellido debe tener al menos 3 caracteres.</p>
          ) : (
            <></>
          )}
          <Input
            type="text"
            placeholder="Apellido"
            name="lastName"
            onChange={handleChange}
            value={formData.lastName}
          />
        </div>
        <div>
          {formData.phone.length < 8 && formData.phone.length >= 1 ? (
            <p>* El telefono debe tener al menos 8 caracteres</p>
          ) : (
            <></>
          )}
          <Input
            type="text"
            placeholder="+54 (9) - 11 - 4552 - 8793"
            name="phone"
            onChange={handleChange}
            value={formData.phone}
          />
        </div>
        <div>
          <Input
            type="email"
            placeholder="Email"
            name="email"
            onChange={handleChange}
            value={formData.email}
          />
        </div>

        {formData.name.length < 3 && err.errName ? (
          <p>* El Nombre debe tener al menos 3 caracteres.</p>
        ) : (
          <></>
        )}
        {formData.lastName.length < 3 && err.errLastName ? (
          <p>* El Apellido debe tener al menos 3 caracteres.</p>
        ) : (
          <></>
        )}
        {formData.phone.length < 8 && err.errPhone ? (
          <p>* El telefono debe tener al menos 8 caracteres</p>
        ) : (
          <></>
        )}
        {(formData.email.length < 1 && err.errEmail) ||
        (!formData.email.includes("@") && formData.email.length > 1) ? (
          <p>* Ingrese un Email valido.</p>
        ) : (
          <></>
        )}

        {!compraOk ? (
          <Container>
            <Button type="submit" disabled={total === 0} onClick={handleSubmit}>
              Enviar Pedido
            </Button>
            <Button onClick={cancel} className="form--clear">
              Cancelar Pedido
            </Button>
          </Container>
        ) : (
          <p>Compra exitosa! Gracias por comprar en Tienda-Rock!!</p>
        )}
      </form>
    </Container>
  );
};

export default Form;
