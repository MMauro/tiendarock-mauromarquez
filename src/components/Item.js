import * as React from "react";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { Button } from "@material-ui/core";
import { Link } from "react-router-dom";
import { Container } from "@mui/material";

const Item = ({ id, title, image, price, stock }) => {
  return (
    <Container>
      <Card sx={{ maxWidth: 345 }}>
        <CardMedia component="img" height="140" image={image} alt={title} />
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            {title}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            Stock disponible: <b>{stock}</b>
          </Typography>
        </CardContent>
        <CardActions>
          <Button
            component={Link}
            to={`/product/${id}`}
            size="small"
            color="primary"
            textDecoration="none"
          >
            Detalles de Producto
          </Button>
        </CardActions>
      </Card>
    </Container>
  );
};

export default Item;
