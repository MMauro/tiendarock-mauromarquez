import RemoveIcon from "@mui/icons-material/Remove";
import AddIcon from "@mui/icons-material/Add";
import React, { useState } from "react";
import { Button, ButtonGroup } from "@material-ui/core";
import { Link } from "react-router-dom";

const ItemCount = ({ item, stockProducto, addItem, initial }) => {
  const [cantidad, setCantidad] = useState(initial);

  const sumaProd = () => {
    if (cantidad < stockProducto) {
      setCantidad(cantidad + 1);
    }
  };

  const restaProd = () => {
    if (cantidad > 0) {
      setCantidad(cantidad - 1);
    }
  };
  return (
    <>
      <div>
        <p>Cantidad que comprará</p>
        <span>
          Stock disponible: <b>{stockProducto - cantidad}</b>
        </span>
        <br></br>
        <br></br>
        <section>
          <ButtonGroup>
            <Button aria-label="reduce" onClick={restaProd}>
              <RemoveIcon fontSize="small" />
            </Button>

            <p>{cantidad}</p>

            <Button aria-label="increase" onClick={sumaProd}>
              <AddIcon fontSize="small" />
            </Button>
          </ButtonGroup>
          {cantidad > 0 ? (
            <Button onClick={() => addItem(item, cantidad)} color="primary">
              AGREGAR AL CARRITO
            </Button>
          ) : (
            <Button disabled={cantidad}>AGREGAR AL CARRITO</Button>
          )}

          <Link to="/cart" disabled={stockProducto === 0}>
            <Button size="small">Ir al carrito</Button>
          </Link>
        </section>
      </div>
    </>
  );
};

export default ItemCount;
