import React from "react";
import ItemCount from "./ItemCount";
import { useState, useContext } from "react";
import { CartContext } from "../context/CartContext";
import { Container } from "@mui/material";

export const ItemDetail = ({ ...product }) => {
  const [add, setAdd] = useState(false);

  const { addItem } = useContext(CartContext);

  return (
    <Container>
      <img src={product.image} alt={`${product.id}-${product.title}`} />
      <section>
        <h1>{product.title}</h1>
        <h2> {product.description}</h2>
        <h2> Precio: ${product.price}</h2>
        {add ? (
          ""
        ) : (
          <ItemCount
            item={product}
            stockProducto={product.stock}
            initial={1}
            addItem={addItem}
          />
        )}
      </section>
    </Container>
  );
};
