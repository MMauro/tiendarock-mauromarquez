import React, { useState, useEffect } from "react";
import { ItemDetail } from "./ItemDetail";
import { useParams } from "react-router-dom";
import CircularProgress from "@mui/material/CircularProgress";
import Box from "@mui/material/Box";
import db from "../firebase/firebase";
import { getDoc, doc } from "firebase/firestore";
import { Container, Grid } from "@mui/material";

export const ItemDetailContainer = ({ greeting }) => {
  const [product, setProduct] = useState({});
  const [loading, setLoading] = useState(true);

  const { itemId } = useParams();

  useEffect(() => {
    setLoading(true);
    const ref = doc(db, "products", itemId);

    getDoc(ref)
      .then((querySnapshot) => {
        setProduct({ ...querySnapshot.data(), id: querySnapshot.id });
      })
      .catch((e) => console.log(e));
    setLoading(false);
  }, []);

  return loading ? (
    <Container maxWidth="lg">
      <Grid container>
        <Grid item xs={12} sm={8}>
          <Box borderBottom={1}>
            <CircularProgress />
            <h2>CARGANDO...</h2>
          </Box>
        </Grid>
      </Grid>
    </Container>
  ) : (
    <>
      <h2>{greeting}</h2>
      <ItemDetail {...product} />
    </>
  );
};
