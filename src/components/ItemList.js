import React from "react";
import Item from "./Item";
import { Container, Grid } from "@mui/material";

export const ItemList = ({ items }) => {
  return (
    <Container>
      <Grid container spacing={2}>
        {items?.map((item) => (
          <Grid item xs={4}>
            <Item {...item} key={item.id} />
          </Grid>
        ))}
      </Grid>
    </Container>
  );
};
