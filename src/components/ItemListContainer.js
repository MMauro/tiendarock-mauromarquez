import { useState, useEffect } from "react";
import { ItemList } from "./ItemList";
import { useParams } from "react-router-dom";
import CircularProgress from "@mui/material/CircularProgress";
import db from "../firebase/firebase";
import { collection, getDocs, query, where } from "firebase/firestore";
import { Container, Grid, Box } from "@mui/material";

const ItemListContainer = ({ greeting }) => {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(true);
  const { catId } = useParams();

  useEffect(async () => {
    setLoading(true);

    const myData = catId
      ? query(collection(db, "products"), where("category", "==", catId))
      : collection(db, "products");

    const querySnapshot = await getDocs(myData);

    setItems(
      querySnapshot.docs.map((el) => {
        return { ...el.data(), id: el.id };
      })
    );

    setLoading(false);
  }, [catId]);

  return loading ? (
    <Container maxWidth="lg">
      <Grid container>
        <Grid item xs={12} sm={8}>
          <Box borderBottom={1}>
            <CircularProgress />
            <h2>CARGANDO...</h2>
          </Box>
        </Grid>
      </Grid>
    </Container>
  ) : (
    <>
      <h1>{greeting}</h1>
      <ItemList items={items} />
    </>
  );
};

export default ItemListContainer;
