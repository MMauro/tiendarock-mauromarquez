import React from "react";
import { AppBar, Toolbar, Typography } from "@material-ui/core";
import Button from "@mui/material/Button";
import { makeStyles } from "@material-ui/core/styles";
import theme from "../temaConfig";
import CartWidget from "./CartWidget";
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  offset: theme.mixins.toolbar,
  title: {
    flexGrow: 1,
  },
}));

export const NavBar = () => {
  const categories = [
    { id: "asfadd", address: "/", text: "HOME" },
    { id: "123asf", address: "/category/A", text: "BUZOS Y REMERAS" },
    { id: "sgs3q3", address: "/category/B", text: "TAZAS PERSONALIZADAS" },
    { id: "gkl98s", address: "/category/C", text: "LIBROS" },
  ];

  const classes = useStyles(theme);

  return (
    <div>
      <AppBar>
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            Tienda-Rock
          </Typography>

          <Typography color="secondary">
            {categories.map((cat) => (
              <Button
                component={Link}
                to={cat.address}
                key={cat.id}
                style={{
                  textDecoration: "none",
                }}
              >
                {cat.text}
              </Button>
            ))}
            <Button
              component={Link}
              to="/cart"
              style={{ textDecoration: "none" }}
            >
              <CartWidget />
            </Button>
          </Typography>
        </Toolbar>
      </AppBar>
      <div className={classes.offset}></div>
    </div>
  );
};
