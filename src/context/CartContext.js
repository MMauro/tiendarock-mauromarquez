import React, { createContext, useState, useEffect } from "react";

export const CartContext = createContext([]);

export const CartProvider = ({ children }) => {
  const [items, setItems] = useState([]);
  const [total, setTotal] = useState(0);

  const isInCart = (id) => {
    const found = items.find((item) => item.id == id);
    return found;
  };

  const addItem = (item, cantidad) => {
    isInCart(item.id)
      ? setItems(
          items.map((prod) => {
            if (prod.id === item.id) {
              prod.cantidad += cantidad;
            }
            return prod;
          })
        )
      : setItems([
          ...items,
          {
            id: item.id,
            title: item.title,
            price: item.price,
            cantidad: cantidad,
          },
        ]);
    console.log(item);
  };

  useEffect(() => {
    let tot = 0;
    items.map((item) => {
      return (tot += item.cantidad * item.price);
    });
    setTotal((prevTotal) => tot);
  }, [items]);

  const removeItem = (id) => {
    setItems(items.filter((item) => item.id !== id));
  };

  const clearItem = () => {
    setItems([]);
  };
  return (
    <CartContext.Provider
      value={{ items, addItem, removeItem, clearItem, total }}
    >
      {children}
    </CartContext.Provider>
  );
};
