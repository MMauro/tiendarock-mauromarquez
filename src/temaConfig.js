import { createTheme } from "@material-ui/core/styles";
import purple from "@material-ui/core/colors/purple";
import lightGreen from "@material-ui/core/colors/lightGreen";
const theme = createTheme({
  palette: {
    primary: {
      main: "#311b92",
    },
    secondary: {
      main: "#9bc0ff",
    },
  },
  link: {
    textDecoration: "none",
    color: "secondary",
  },
});

export default theme;
